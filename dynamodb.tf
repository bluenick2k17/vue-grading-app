resource "aws_dynamodb_table" "vue-grading-db" {
    name = "Grades"
    billing_mode = "PAY_PER_REQUEST"
    
    attribute {
      name = "guid"
      type = "S"
    }

    hash_key = "guid"
}