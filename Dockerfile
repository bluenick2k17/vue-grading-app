FROM node:16
WORKDIR /usr/src/app

# Copy the source code over
COPY . .

# Get dependencies
RUN npm ci --only=production --prefix ./express/
RUN npm ci --prefix ./static-ui/

# Build the UI
RUN npm run build-ui --prefix ./express/

# Remove the extra static UI stuff
RUN rm -rf ./static-ui/

# Open the necessary ports
EXPOSE 3000
CMD [ "node", "./express/index.js" ]