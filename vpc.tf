# Configure VPC

# Providing a reference to our default VPC
resource "aws_default_vpc" "default_vpc" {
}

# Providing a reference to our default subnets
resource "aws_default_subnet" "default_subnet_a" {
  availability_zone = "${var.AWS_REGION}a"
}

resource "aws_default_subnet" "default_subnet_b" {
  availability_zone = "${var.AWS_REGION}b"
}

resource "aws_default_subnet" "default_subnet_c" {
  availability_zone = "${var.AWS_REGION}c"
}

# Configure load balancer (this also contains our public IP)
resource "aws_alb" "ecs_load_balancer" {
    name = "vue-grading-lb"
    load_balancer_type = "application"
    subnets = local.subnet_list
    security_groups = [aws_security_group.load_balancer_security_group.id]
}

resource "aws_security_group" "load_balancer_security_group" {
    ingress {
    from_port   = 80 # Allowing traffic in from port 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic in from all sources
  }

  egress {
    from_port   = 0 # Allowing any incoming port
    to_port     = 0 # Allowing any outgoing port
    protocol    = "-1" # Allowing any outgoing protocol 
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
  }
}

resource "aws_lb_target_group" "lb_target" {
    name = "lb-target"
    port = 80
    protocol = "HTTP" # consider setting up HTTPS in the future
    target_type = "ip"
    vpc_id = aws_default_vpc.default_vpc.id
    health_check {
        matcher = "200"
        path = "/"
    }
}

resource "aws_lb_listener" "lb_listener" {
    load_balancer_arn = aws_alb.ecs_load_balancer.arn
    port = 80
    protocol = "HTTP"
    default_action {
        type             = "forward"
        target_group_arn = aws_lb_target_group.lb_target.arn
    }
}