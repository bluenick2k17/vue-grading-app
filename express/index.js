const debug = require('debug')('index.js')
const express = require('express');
const morgan = require('morgan');
const html5history = require('connect-history-api-fallback');
const bodyparser = require('body-parser');
const multer = require('multer');
const { Readable } = require('stream');
const csv = require('csv-parser');
const upload = multer();
const cors = require('cors');
const { DynamoDBClient } = require('@aws-sdk/client-dynamodb');
const { DynamoDBDocument } = require('@aws-sdk/lib-dynamodb');

//Initialize DB
let bad_db;
const server = express();

if (process.env.NODE_LOCAL){
    debug('local environment!')
    bad_db = new DynamoDBClient({endpoint: "http://localhost:8000"});
    server.use(cors({origin: 'http://localhost:8080'}));
} else {
    bad_db = new DynamoDBClient({region: "us-east-1"});
}

const dynamo = new DynamoDBDocument(bad_db);

//TABLE DEFINITION
/*

aws dynamodb create-table --table-name Grades --attribute-definitions AttributeName=guid,AttributeType=S --key-schema AttributeName=guid,KeyType=HASH --billing-mode PAY_PER_REQUEST --endpoint http://localhost:8000

*/
//Define my database
//"Display ID", "ID", "Last Name", "First Name", "grade", "Submission date", "Late submission"
//dynamo.AWS.config.update({endpoint: "http://localhost:8000"})

const staticPath = __dirname + '/views';
server.use(morgan('combined'));
server.use(html5history());
server.use(express.static(staticPath));

server.post('/api/csvupload', upload.single('grades_csv'), function(req, res) {

    //STEPS
    //- take in the csv
    //- validate it?
    //- create the list of students
    //- upload to dynamodb

    //FIXME check for GUID
    if (req.body.form_guid === 'undefined'){
        res.status(400).send("Request requires GUID.");
    } else {
        let guid = req.body.form_guid;
        const results = [];
        Readable.from(req.file.buffer)
            .pipe(csv({
                skipLines: 3,
                headers: [ "Display ID", "ID", "Last Name", "First Name", "grade", "Submission date", "Late submission" ]
            }))
            .on('data', (data) => results.push(data))
            .on('end', async () => {
                //Upload to DynamoDB
                var dynamoEntry =
                    {
                        TableName: "Grades",
                        Item: {
                            guid,
                            StuGrades: results
                        },
                        ReturnValues: "NONE",
                    };

                await dynamo.put(dynamoEntry)
                    .then(data => {
                        console.log(data)
                        res.send('Received request');
                    }).catch(err => {
                        console.log(err)
                        res.status(500).send(err);
                    });
            });
    }
});

server.post('/api/submitnew', bodyparser.json(), async function(req, res) {
    //FIXME add validation

    if (req.body.form_guid === 'undefined'){
        res.status(400).send("Request requires GUID.");
    } else {
        let guid = req.body.form_guid;
        let submittedData = req.body;

        let dynamoUpdateEntry =
            {
                TableName: "Grades",
                Key: { guid },
                ExpressionAttributeValues: {
                    ":Assignment": submittedData
                },
                ReturnValues: "NONE",
                UpdateExpression: "SET AssignmentInfo = :Assignment",
            };

            await dynamo.update(dynamoUpdateEntry)
            .then(data => {
                console.log(data)
                res.send('Received request');
            }).catch(err => {
                console.log(err)
                res.status(500).send(err);
            });
    }
});

server.post('/api/gradedzipupload', upload.single('zip_file'), function(req, res) {

    //probably need to write to a file, then use adm-zip to read it again and process it
    //STEPS
    //- write to file, read into adm
    //- validate that the zip has the correct structure?
    //- change submitted attachments to feedback attachments
    //- generate comments and put in zip file
    //- fill in grades
    //- give zip file to download back to the user

    res.send('Received request');
    //res.status(401).send('Some error');
});

server.listen(3000, () => {
    debug('Server is up!');
});