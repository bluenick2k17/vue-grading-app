variable "GITLAB_USERNAME" {
    type = string
    default = "bluenick2k15"
}

variable "GITLAB_REGISTRY_KEY" {
    type = string
}

variable "AWS_REGION"{
    type = string
    default = "us-east-1"
}

locals {
    subnet_list = ["${aws_default_subnet.default_subnet_a.id}", "${aws_default_subnet.default_subnet_b.id}", "${aws_default_subnet.default_subnet_c.id}"]
}