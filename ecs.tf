resource "aws_ecs_task_definition" "grading-service" {
  family             = "vue-grading-app"
  execution_role_arn = aws_iam_role.vue_grading_ecs_exec.arn
  task_role_arn = aws_iam_role.vue_grading_ecs_task.arn
  container_definitions = jsonencode([
    {
      name : "vue-grading-app"
      image : "registry.gitlab.com/bluenick2k17/vue-grading-app:latest"
      repositoryCredentials : {
        credentialsParameter : aws_secretsmanager_secret.gitlab_registry_key.id
      }
      cpu : 256
      memory : 512
      portMappings : [
        {
          containerPort = 3000
          hostPort      = 3000
        }
      ]
      essential = true
      # logConfiguration = {
      #   "logDriver" = "awslogs",
      #   "options" = {
      #       "awslogs-group": "awslogs-vuegrading",
      #       "awslogs-region": "${var.AWS_REGION}",
      #       "awslogs-stream-prefix": "awslogs-vue"
      #   }
      # }
    } # use gitlab container registry, set up creds through aws secrets manager I guess?
  ])
  requires_compatibilities = [ "FARGATE" ]
  network_mode = "awsvpc"
  cpu = 256
  memory = 512
}

resource "aws_ecs_cluster" "grading-service-cluster" {
  name = "grading-service-cluster"
}

resource "aws_ecs_service" "ecs-grading-service" {
  name            = "ecs-grading-service"
  cluster         = aws_ecs_cluster.grading-service-cluster.id
  task_definition = aws_ecs_task_definition.grading-service.arn
  launch_type     = "FARGATE"
  desired_count   = 1
  wait_for_steady_state = true

  network_configuration {
    subnets          = local.subnet_list
    assign_public_ip = true # Providing our containers with public IPs
    security_groups = [aws_security_group.ecs_security_group.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.lb_target.arn
    container_name = aws_ecs_task_definition.grading-service.family
    container_port = 3000
  }
}

resource "aws_security_group" "ecs_security_group" {
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = [aws_security_group.load_balancer_security_group.id]
  }

  egress {
    from_port   = 0 # Allowing any incoming port
    to_port     = 0 # Allowing any outgoing port
    protocol    = "-1" # Allowing any outgoing protocol 
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
  }
}