terraform {
  # backend "remote" {
  #   organization = "example-org-a9a9b9"
  #   workspaces {
  #     name = "Grading-App"
  #   }
  # }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

}

output "load_balancer_address" {
  value = aws_alb.ecs_load_balancer.dns_name
  description = "Address to use to connect to the load balancer."
}

output "dynamo_table" {
  value = aws_dynamodb_table.vue-grading-db.arn
  description = "DB ARN."
}

provider "aws" {
  profile = "default"
  region  = "${var.AWS_REGION}"
}

resource "aws_secretsmanager_secret" "gitlab_registry_key" {
  name        = "Gitlab_Registry_Key"
  description = "Private credential used to retrieve info from the Gitlab Container Registry"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "gr_secret" {
  secret_id = aws_secretsmanager_secret.gitlab_registry_key.id
  secret_string = jsonencode({
    username = var.GITLAB_USERNAME
    password = var.GITLAB_REGISTRY_KEY
  })
}

resource "aws_iam_role" "vue_grading_ecs_exec" {
  name = "vue_grading_ecs_exec"
  assume_role_policy = data.aws_iam_policy_document.ecs_assume_role_policy.json
}

resource "aws_iam_role" "vue_grading_ecs_task" {
  name = "vue_grading_ecs_task"
  assume_role_policy = data.aws_iam_policy_document.ecs_assume_role_policy.json
}

data "aws_iam_policy_document" "ecs_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.vue_grading_ecs_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy" "gpr_access_secret" {
  name = "gpr_access_secret"
  role = aws_iam_role.vue_grading_ecs_exec.name
  policy = data.aws_iam_policy_document.gpr_access_secret_policy.json
}

data "aws_iam_policy_document" "gpr_access_secret_policy" {
  statement {
    effect = "Allow"

    actions = [ "secretsmanager:GetSecretValue" ]

    resources = [ aws_secretsmanager_secret.gitlab_registry_key.id ]
  }
}

resource "aws_iam_role_policy" "gpr_access_dynamodb" {
  name = "gpr_access_dynamodb"
  role = aws_iam_role.vue_grading_ecs_task.name
  policy = data.aws_iam_policy_document.gpr_access_dynamodb_policy.json
}

data "aws_iam_policy_document" "gpr_access_dynamodb_policy" {
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:UpdateItem"
      ]

    resources = [ aws_dynamodb_table.vue-grading-db.arn ]
  }
}